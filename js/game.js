//L Piece Build
function LPiece()
{
    this.state1 = [
        [1, 0],
        [1, 0],
        [1, 1]
    ];

    this.state2 = [
        [0, 0, 1],
        [1, 1, 1]
    ];

    this.state3 = [
        [1, 1],
        [0, 1],
        [0, 1]
    ];

    this.state4 = [
        [1, 1, 1],
        [1, 0, 0]
    ];

    this.states = [ this.state1, this.state2, this.state3, this.state4 ];
    this.curState = 0;

    this.color = 0;
    this.gridx = 4;
    this.gridy = -3;

    this.type = "L";
    this.reverse = false;
}

//Reverse L Piece Build
function ReverseLPiece()
{
    this.state1 = [ [0, 1],
        [0, 1],
        [1, 1] ];

    this.state2 = [ [1, 1, 1],
        [0, 0, 1] ];

    this.state3 = [ [1, 1],
        [1, 0],
        [1, 0] ];

    this.state4 = [ [1, 0, 0],
        [1, 1, 1] ];

    this.states = [ this.state1, this.state2, this.state3, this.state4 ];
    this.curState = 0;

    this.color = 0;
    this.gridx = 4;
    this.gridy = -3;

    this.type = "L";
    this.reverse = true;
}

//Block Piece Build
function BlockPiece()
{
    this.state1 = [ [1, 1],
        [1, 1] ];

    this.states = [ this.state1 ];
    this.curState = 0;

    this.color = 0;
    this.gridx = 4;
    this.gridy = -2;

    this.type = "Box";
    this.reverse = null;
}

//Line Piece Build
function LinePiece()
{
    this.state1 = [ [1],
        [1],
        [1],
        [1] ];

    this.state2 = [ [1,1,1,1] ];

    this.states = [ this.state1, this.state2 ];
    this.curState = 0;

    this.color = 0;
    this.gridx = 5;
    this.gridy = -4;

    this.type = "Line";
    this.reverse = null;
}


//T Piece Build
function TPiece()
{
    this.state1 = [ [1, 1, 1],
        [0, 1, 0] ];

    this.state2 = [ [1, 0],
        [1, 1],
        [1, 0] ];

    this.state3 = [ [0, 1, 0],
        [1, 1, 1] ];

    this.state4 = [ [0, 1],
        [1, 1],
        [0, 1] ];

    this.states = [ this.state1, this.state2, this.state3, this.state4 ];
    this.curState = 0;

    this.color = 0;
    this.gridx = 4;
    this.gridy = -2;

    this.type = "T";
    this.reverse = null;

}

//Z Piece Build
function ZPiece()
{
    this.state1 = [ [1, 1, 0],
        [0, 1, 1] ];

    this.state2 = [ [0, 1],
        [1, 1],
        [1, 0] ];

    this.states = [ this.state1, this.state2 ];
    this.curState = 0;

    this.color = 0;
    this.gridx = 4;
    this.gridy = -2;

    this.type = "Z";
    this.reverse = false;
}

//Reverse Z Piece Build
function ReverseZPiece()
{
    this.state1 = [ [0, 1, 1],
        [1, 1, 0] ];

    this.state2 = [ [1, 0],
        [1, 1],
        [0, 1] ];

    this.states = [ this.state1, this.state2 ];
    this.curState = 0;

    this.color = 0;
    this.gridx = 4;
    this.gridy = -2;

    this.type = "Z";
    this.reverse = true;
}

function Pieces(){
    //returns a random piece
    this.getRandomPiece = function(){
        var result = Math.floor( Math.random() * 7 );
        var piece;

        switch(result)
        {
            case 0:
                piece = new LPiece();
                break;

            case 1:
                piece = new ReverseLPiece();
                break;

            case 2:
                piece = new BlockPiece();
                break;

            case 3:
                piece = new LinePiece();
                break;

            case 4:
                piece = new TPiece();
                break;

            case 5:
                piece = new ZPiece();
                break;

            case 6:
                piece = new ReverseZPiece();
                break;
        }

        piece.color = Math.floor(Math.random() * 8);
        return piece;
    };
}

//perform function on click of specified class
function forEachClassDoOnClick(className, action){
    var el = document.getElementsByClassName(className);
    for(var i = 0; i < el.length; i++){
        el[i].addEventListener('click', function(e){
            e.preventDefault();
            action();
        });
    }
}


//for every dom el of classname, perform action
function forEachClass(className, action){
    var el = document.getElementsByClassName(className);
    for(var i = 0; i < el.length; i++){
        action(el, i);
    }
}

//initial game variables
function NgTetrJS(){

    this.rows = 20;
    this.cols = 10;
    this.size = 32;
    this.pause = false;
    this.timeDiff = 500;
    this.pieces;
    this.canvas;
    this.ctx;
    this.pauseBtn;
    this.imgLoader;
    this.nextPiece;
    this.blockImg;
    this.bgImg;
    this.gameOverImg;
    this.curPiece;
    this.gameData;
    this.imgLoader;
    this.prevTime;
    this.curTime;
    this.isGameOver;
    this.lineSpan;
    this.curLines;
    this.touchX;
    this.touchY;
    this.touchId;
    //after every 10 lines, will increase the speed and then increase the level
    this.counter = 0;
    this.level = 0;
    this.score = 0;
    this.counterHTML;
    this.levelHTML;
    this.scoreHTML;
    this.previewCanvas;
    this.previewCanvasCtx;
    this.touchX;
    this.touchY;
    this.touchId;

    this.removingLines = false;
    this.canBringDownNextPiece;

    this.gameOverSound;
    this.linesFoundSound;
    this.movePieceSound;
}


function doFuncOnTagEvent(tag, event, func){
    var theTag = document.getElementsByTagName(tag);
    for(var i = 0; i < theTag.length; i++){
        theTag[i].addEventListener(event, function(){
            func();
        });
    }
}
//boots the game
NgTetrJS.prototype.init = function(){
  this.states().start();
};


//game states
NgTetrJS.prototype.states = function(){
  var tetrjs = this;
  return {
      start: function(){tetrjs.startState()},
      game: function(){tetrjs.gameState()},
      controls: function(){tetrjs.controlState()},
      end: function(){tetrjs.endState()}
  };
};

//displays the controls information state
NgTetrJS.prototype.controlState = function(){
    this.displayStateEl("controlState");
};

//start screen
NgTetrJS.prototype.startState = function(){

    this.displayStateEl("startState");

    this.setCanvas();

    var tetrjs = this;

    doFuncOnTagEvent('a', 'click', function(){
        tetrjs.playSound('movepiece.mp3');
    });



    forEachClassDoOnClick("start", function(){
        tetrjs.states().game();
    });

    forEachClassDoOnClick("controls", function(){
        tetrjs.states().controls();
    });

    forEachClassDoOnClick("continue", function(){
        tetrjs.playGame();
    });

    forEachClassDoOnClick("quit", function(){
        tetrjs.quitGame();
    });

    forEachClassDoOnClick("start-state", function(){
       tetrjs.states().start();
    });

    forEachClassDoOnClick('menu', function(){
        tetrjs.states().start();
    });

    document.getElementById("clickthis").addEventListener("click", function(){
        tetrjs.gameOverSound.play();
    });
};


//Hides all states and shows specified state
NgTetrJS.prototype.displayStateEl = function(whichState){
    forEachClass('states', function(el, i){
        (el[i].id == whichState ?  el[i].style.display = "block" :  el[i].style.display = "none");
    });
};

//game Over Screen
NgTetrJS.prototype.endState = function(){
    this.updatePlayerStats();
    this.displayStateEl("endState");

    this.pause = false;
    this.gameOverSound.play();

};


//returns an object of player stats, to be used on game end to send to FireBase
NgTetrJS.prototype.getPlayerStats = function(){
    var tetrjs = this;
    return {
        score: tetrjs.score,
        lines: tetrjs.curLines,
        level: tetrjs.level
    };
};

//writes to the screen the players score, levels, lines
NgTetrJS.prototype.updatePlayerStats = function(){
    var tetrjs = this;
    forEachClass('endLines', function(el, i){
        el[i].innerHTML = tetrjs.curLines.toString();
    });

    forEachClass('endLevels', function(el, i){
        el[i].innerHTML = tetrjs.level.toString();
    });

    forEachClass('endScore', function(el, i){
        el[i].innerHTML = tetrjs.score.toString();
    });
};


//inits the game state
NgTetrJS.prototype.gameState = function(){

    var tetrjs = this;

    forEachClass('endLines', function(el, i){
        el[i].innerHTML = '0';
    });

    forEachClass('endLevels', function(el, i){
        el[i].innerHTML = '0';
    });

    forEachClass('endScore', function(el, i){
        el[i].innerHTML = '0';
    });




    this.displayStateEl('');
    this.onReady();
};




//set and load game images
NgTetrJS.prototype.setImages = function(){
    this.imgLoader.addImage("blocks.png", "blocks");
    this.imgLoader.addImage("bg.png", "bg");
    this.imgLoader.addImage("over.png", "gameover");
    this.imgLoader.loadImages();
};

//initialises game settings
NgTetrJS.prototype.setGameVars = function(){

    this.imgLoader = new BulkImageLoader();

    this.setCanvas();


    this.pauseBtn = document.getElementById("pauseTetrjs");
    this.lineSpan = document.getElementById("lines");
    this.timeDiff = 500;
    this.score = 0;
    this.level = 0;
    this.counter = 0;
    this.counterHTML = document.getElementById("counter");
    this.levelHTML = document.getElementById("level");
    this.scoreHTML = document.getElementById("points");


    this.previewCanvas = document.getElementById("previewPiece");
    this.previewCanvasCtx = this.previewCanvas.getContext("2d");


    this.canBringDownNextPiece = true;

    this.gameOverSound = new Audio("sound/gameover.mp3");
    this.linesFoundSound = new Audio("sound/linesfound.mp3");
    this.movePieceSound = new Audio("sound/movepiece.mp3");

};



//sets the game canvas
NgTetrJS.prototype.setCanvas = function(){
    this.canvas = document.getElementById("gameCanvas");
    this.ctx = this.canvas.getContext("2d");
};



//prepares the game by setting game variables and setting images
NgTetrJS.prototype.onReady = function(){


    this.setGameVars();
    this.imgLoader = new BulkImageLoader();
    this.canvas = document.getElementById("gameCanvas");
    this.ctx = this.canvas.getContext("2d");
    this.pauseBtn = document.getElementById("pauseTetrjs");
    this.timeDiff = 500;
    this.prevTime = this.curTime = 0;

    this.ctx.clearRect(0,0,320,640);



    //dothing

    this.setImages();

    var tetrjs = this;

    //sets the images and initialises the game
    this.imgLoader.onReadyCallback = function(){
        tetrjs.blockImg = tetrjs.imgLoader.getImageAtIndex(0);
        tetrjs.initGame();

        document.onkeydown = function(e){
            tetrjs.getInput(e);
        };

        //tetrjs.touchListeners();

        document.getElementById("pauseTetrjs").addEventListener("click", function(){
            tetrjs.pauseGame();
        });

    };

    this.canvas.addEventListener('click', function(){
        tetrjs.userAction().pushDown();
    });

    document.getElementById("keyup").addEventListener( 'click', function(){
        tetrjs.userAction().switchPiece();
    });

    document.getElementById("keydown").addEventListener( 'click', function(){
        tetrjs.userAction().down();
    });

    document.getElementById("keyleft").addEventListener( 'click', function(){
        tetrjs.userAction().left();
    });

    document.getElementById("keyright").addEventListener( 'click', function(){
        tetrjs.userAction().right();
    });



};



//object of move piece methods
NgTetrJS.prototype.userAction = function(){
    var tetrjs = this;
    return {
        left: function(){
            if(tetrjs.checkMove(
                    tetrjs.curPiece.gridx - 1,
                    tetrjs.curPiece.gridy,
                    tetrjs.curPiece.curState
                )){
                tetrjs.curPiece.gridx--;
            }
        },
        right: function(){
            if(tetrjs.checkMove(
                    tetrjs.curPiece.gridx + 1,
                    tetrjs.curPiece.gridy,
                    tetrjs.curPiece.curState
                )){
                tetrjs.curPiece.gridx++;
            }
        },
        down: function(){
            if(tetrjs.checkMove(
                    tetrjs.curPiece.gridx,
                    tetrjs.curPiece.gridy + 1,
                    tetrjs.curPiece.curState
                )){
                tetrjs.curPiece.gridy++;
                tetrjs.score += 1;
                tetrjs.updatePlayerStats();

            }
        },
        switchPiece: function(){
            var newstate = tetrjs.curPiece.curState - 1;
            if(newstate < 0){
                newstate = tetrjs.curPiece.states.length -1;
            }
            if(
                tetrjs.checkMove(
                    tetrjs.curPiece.gridx, tetrjs.curPiece.gridy, newstate
                )

            ){
                tetrjs.curPiece.curState = newstate;
                tetrjs.playSound('movepiece.mp3');
            }

            //if at the edge, then bring into a suitable position for the piece, in order to do the switch

            tetrjs.ifIsFarRight(function(){
                if(tetrjs.curPiece.type == "Line"){
                    tetrjs.curPiece.gridx = tetrjs.cols - 4;
                    tetrjs.curPiece.curState = newstate;
                    tetrjs.playSound('movepiece.mp3');
                } else {
                    tetrjs.curPiece.gridx--;
                    tetrjs.curPiece.curState = newstate;
                    tetrjs.playSound('movepiece.mp3');
                }
            });
        },

        pushDown: function(){
            tetrjs.pushToBottom();
            tetrjs.updatePlayerStats();
            tetrjs.playSound('movepiece.mp3');
        }
    }
};

//responds to user input
NgTetrJS.prototype.getInput = function(e){

    if(!e){
        var e = window.event;
    }
    e.preventDefault();


    if(this.isGameOver != true && this.pause != true){

        switch(e.keyCode){
            case 37://left
                this.userAction().left();
            break;

            case 39://right
                this.userAction().right();
            break;


            case 40: //down
                this.userAction().down();
            break;

            case 38: //switch
                this.userAction().switchPiece();
            break;

            case 32: //put piece all the way down
                this.userAction().pushDown();
            break;
        }

    }

};

//listens for screen touches
NgTetrJS.prototype.touchListeners = function(){



    var tetrjs = this;
    document.body.addEventListener('touchstart', function(e){
        e.preventDefault();
        touchX = e.touches[0].pageX;
        touchY = e.touches[0].pageY;
        touchId = e.touches[0].identifier;
    });


    document.body.addEventListener('touchmove', function(e){
        e.preventDefault();
        var difY = e.touches[0].pageY - tetrjs.touchY;
        if(difY > 60){
            if(tetrjs.checkMove(
                    tetrjs.curPiece.gridx,
                    tetrjs.curPiece.gridy + 1,
                    tetrjs.curPiece.curState
                )){
                tetrjs.curPiece.gridy++;
            }
        }
    });


    document.body.addEventListener('touchend', function(e){
        e.preventDefault();

        var touchEndX;
        var touchEndY;

        var touch = e.changedTouches.item(0);


        try{
            touchEndX = touch.pageX;
            touchEndY = touch.pageY;
        } catch(err){
            console.log(err);
            return;
        }

        var difX = Math.abs(touchEndX - tetrjs.touchX);
        var difY = Math.abs(touchEndY - tetrjs.touchY);

        if(difX < 10 && difY < 10){
            var newState = tetrjs.curPiece.curState -1;
            if(newState){
                newState = tetrjs.curPiece.states.length - 1;
            }
            if(tetrjs.checkMove(
                tetrjs.gridx - 1,
                tetrjs.gridy,
                tetrjs.curPiece.curState
                )){
                tetrjs.curPiece.gridx--;
            } else {
                if(tetrjs.checkMove(
                        tetrjs.curPiece.gridx + 1,
                        tetrjs.curPiece.gridy,
                        tetrjs.curPiece.curState
                    )){
                    tetrjs.curPiece.gridx++;
                }
            }
        }


    });




};


//Pushes current piece to the bottom of the board
NgTetrJS.prototype.pushToBottom = function(){

        while(this.checkMove(
                this.curPiece.gridx,
                this.curPiece.gridy + 1,
                this.curPiece.curState)
        ){
            this.curPiece.gridy += 1;
            this.score += 1;
        }

        this.copyData(this.curPiece);
        this.curPiece = this.nextPiece;

        this.nextPiece = this.pieces.getRandomPiece();
        this.drawNextPiecePreview(this.nextPiece);
        document.getElementById("nextPiece").innerHTML = this.nextPiece.type.toString();

        this.updatePlayerStats();

};


//initialises the game
NgTetrJS.prototype.initGame = function(){
    console.log('init method');
    var r,c;
    this.curLines = 0;
    this.isGameOver = false;

    //builds the game board
    if(this.gameData == undefined){
        this.gameData = new Array();
        for(r = 0; r < this.rows; r++){
            //each row is an array for the columns
            this.gameData[r] = new Array();
            for(c = 0; c < this.cols; c++){
                this.gameData[r].push(0);//give row a blank value
            }
        }

    } else {
        for(r = 0; r < this.rows; r++){
            for(c = 0; c < this.cols; c++){
                this.gameData[r][c] = 0;
            }
        }
    }
    this.pieces = new Pieces();
    this.curPiece = this.pieces.getRandomPiece();
    this.nextPiece = this.pieces.getRandomPiece();
    this.drawNextPiecePreview(this.nextPiece);
    document.getElementById("nextPiece").innerHTML = this.nextPiece.type.toString();


    //fallbacks for animation frame
    var requestAnimFrame =
        window.requestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.msRequestAnimationFrame;

    window.requestAnimationFrame = requestAnimFrame;

    //begins the loop
    var tetrjs = this;
    requestAnimationFrame(function(){
        tetrjs.update()
    });



};

//Main Game Loop, brings down piece, checks for lines,
NgTetrJS.prototype.update = function(){


    if(this.pause == false){

        //store instance of current time
        this.curTime = new Date().getTime();


        //if time diff has been passed
        if((this.curTime - this.prevTime) > this.timeDiff){


            if(this.checkMove(
                    this.curPiece.gridx,
                    this.curPiece.gridy + 1,
                    this.curPiece.curState)
            ){

                this.curPiece.gridy += 1;

            } else {
                this.playSound('movepiece.mp3');
                this.copyData(this.curPiece);
                this.curPiece = this.nextPiece;
                this.nextPiece = this.pieces.getRandomPiece();
                this.drawNextPiecePreview(this.nextPiece);
                document.getElementById("nextPiece").innerHTML = this.nextPiece.type.toString();
            }

            //update the time
            this.prevTime = this.curTime;

        }


        //clear and re-draw canvas
        this.ctx.clearRect(0,0,320,640);
        this.drawBoard();
        this.drawPiece(this.curPiece, this.curPiece.curState);

    }

    //loop through function until game over occurs
    if(this.isGameOver == false){

        var tetrjs = this;
        requestAnimationFrame(function(){
            tetrjs.update()
        });

    } else if (this.isGameOver == true){

        this.states().end();
    }


};





NgTetrJS.prototype.copyData = function(p){
    var xpos = p.gridx;
    var ypos = p.gridy;
    var state = p.curState;


    for(var r = 0, len = p.states[state].length; r < len; r++){
        for(var c = 0, len2 = p.states[state][r].length; c < len2; c++){

            //if part of that array is 1 &&
            if(p.states[state][r][c] == 1 && ypos >= 0){
                this.gameData[ypos][xpos] = (p.color + 1);
            }

            xpos += 1;

        }
        xpos = p.gridx;
        ypos += 1;
    }

    this.checkForLines();


    if(p.gridy < 0){
        this.isGameOver = true;
    }
};


//Bool that tells if the piece is far right - NOT IN USE
NgTetrJS.prototype.isFarRight = function(){

    var isFarRight = false;
    var newx = this.curPiece.gridx;

    for(var r = 0, len = this.curPiece.states[this.curPiece.curState].length; r < len; r++)
    {
        for(var c = 0, len2 = this.curPiece.states[this.curPiece.curState][r].length; c < len2; c++)
        {

            if(this.curPiece.type == "Line"){
                if(newx >= this.cols - 1 || newx >= this.cols - 2 || newx >= this.cols - 3)
                {
                    c = len2;
                    r = len;
                    isFarRight = true;

                }
            }


            if(newx >= this.cols - 1)
            {
                c = len2;
                r = len;
                isFarRight = true;

            }



            newx += 1;
        }

        newx = this.curPiece.gridx;

    }

    return isFarRight;
};


//Execute an function in the condition that the current piece is far left of the board.
NgTetrJS.prototype.ifIsFarRight = function(action){

    var isFarRight = false;
    var newx = this.curPiece.gridx;

    for(var r = 0, len = this.curPiece.states[this.curPiece.curState].length; r < len; r++)
    {
        for(var c = 0, len2 = this.curPiece.states[this.curPiece.curState][r].length; c < len2; c++)
        {

            if(this.curPiece.type == "Line"){
                if(newx >= this.cols - 1 || newx >= this.cols - 2 || newx >= this.cols - 3)
                {
                    c = len2;
                    r = len;
                    isFarRight = true;
                    action();

                }
            }

            if(newx >= this.cols - 1)
            {
                c = len2;
                r = len;
                //isFarRight = true;
                action();

            }



            newx += 1;
        }

        newx = this.curPiece.gridx;

    }

};

//checks if user can go to specified distance
NgTetrJS.prototype.checkMove = function(xpos, ypos, newState){

    var result = true;
    var newx = xpos;
    var newy = ypos;

    for(var r = 0, len = this.curPiece.states[newState].length; r < len; r++)
    {
        for(var c = 0, len2 = this.curPiece.states[newState][r].length; c < len2; c++)
        {
            if(newx < 0 || newx >= this.cols)
            {
                result = false;
                c = len2;
                r = len;
            }



            if(
                this.gameData[newy] != undefined &&
                this.gameData[newy][newx] != 0 &&
                this.curPiece.states[newState][r] != undefined &&
                this.curPiece.states[newState][r][c] != 0
            )
            {
                result = false;
                c = len2;
                r = len;
            }

            newx += 1;
        }

        newx = xpos;
        newy += 1;

        if(newy > this.rows)
        {
            r = len;
            result = false;
        }
    }

    return result;

};



//updates the score, level, speed
NgTetrJS.prototype.linesFound = function(linesFound){

    this.counter += linesFound;
    this.score += 150;

    if(this.counter >= 10){
        this.level += 1;
        this.counter -= 10;
        this.score += 300;

        this.timeDiff -= 10;

    }

};

//Removes the row if all sections of it are > than 0
NgTetrJS.prototype.zeroRow = function(row){

    var r = row;
    var c = 0;
    while(r >= 0){
        while(c < this.cols){
            if(r > 0){
               this.gameData[r][c] = this.gameData[r-1][c];
            } else {
                this.gameData[r][c] = 0;
            }
            c++;
        }
        c = 0;
        r--;
    }

};

//Checks for for lines and Builds an array of columns that are ready to be removed
NgTetrJS.prototype.checkForLines = function(){


  var rowsArray = new Array();
  var rowsToLoop = (this.rows - 1);
  var colsToLoop = (this.cols);



    while(rowsToLoop >= 0){
        var linesFound = true;

        for(var i = 0; i < colsToLoop; i++){

            if(this.gameData[rowsToLoop][i] <= 0){
                linesFound = false;
            }
        }


        if(linesFound == true){
            rowsArray.push(rowsToLoop);
        }

        rowsToLoop--;
    }

    if(rowsArray.length > 0){

        this.clearLines(rowsArray);
    }

};

//plays a specified file in the sound directory
NgTetrJS.prototype.playSound = function(fileName, external){
    var dir;
    (!external ? dir = 'sound/' : 'http://alexartlett.com/ng-tetris/sound/');

    var fileToPlay = new Audio(dir + fileName);
    fileToPlay.play();
};

//clears the lines, adds a slight animative state
NgTetrJS.prototype.clearLines = function(lines){

    this.playSound('linesfound.mp3');


    lines.reverse();

    var tetrjs = this;

    tetrjs.changeLineColors(lines,3);

    setTimeout(function(){
        tetrjs.changeLineColors(lines, 1);

    }, 100);
    setTimeout(function(){
        tetrjs.changeLineColors(lines, 3);

    }, 150);


    setTimeout(function(){
        for(var i = 0; i < lines.length; i++){

            tetrjs.zeroRow(lines[i]);
        }
        tetrjs.canBringDownNextPiece = true;

    }, 200);


    tetrjs.curLines += lines.length;
    this.linesFound(lines.length);
    this.updatePlayerStats();
};


//loops through a whole block of lines made by player and changes the color
NgTetrJS.prototype.changeLineColors = function(lines, colorNum){
    for(var i = 0; i < lines.length; i++){
        for(var j = 0; j < tetrjs.gameData[lines[i]].length; j++){
            console.log(tetrjs.gameData[lines[i]][j]);
            tetrjs.gameData[lines[i]][j] = colorNum;
        }
    }
};

//Draws the board
NgTetrJS.prototype.drawBoard = function(){

    for(var r = 0; r < this.rows; r++){
        for(var c = 0; c < this.cols; c++){
            if(this.gameData[r][c] != 0){
                this.ctx.drawImage(
                    this.blockImg,
                    (this.gameData[r][c] - 1) * this.size,
                    0,
                    this.size,
                    this.size,
                    c * this.size,
                    r * this.size,
                    this.size,
                    this.size
                );
            }
        }
    }



};

//Draws specified Piece
NgTetrJS.prototype.drawPiece = function(p){

    var drawX = p.gridx;
    var drawY = p.gridy;
    var state = p.curState;

    for(var r = 0, len = p.states[state].length; r < len; r++){
        for(var c = 0, len2 = p.states[state][r].length; c < len2; c++){

            //if part in array piece has a 1 and  gridy is > 0
            if(p.states[state][r][c] == 1 && drawY >= 0){
                this.ctx.drawImage(
                    this.blockImg,
                    p.color * this.size,
                    0,
                    this.size,
                    this.size,
                    drawX * this.size,
                    drawY * this.size,
                    this.size,
                    this.size
                );
            }
            drawX += 1;

        }
        drawX = p.gridx;
        drawY += 1;
    }

};

//draws the preview of the next piece to the preview canvas
NgTetrJS.prototype.drawNextPiecePreview = function(p){

    var drawX = p.gridx;
    var drawY = 0.5;
    var state = p.curState;


    var tetrjs = this;
    //sets a x val based on the piece type, to help center in the middle of the preview
    function drawXFunc(){
        if(p.type == "Box" || p.type == "L"){

                return (drawX * tetrjs.size) - 95;

        } else {
            return (drawX * tetrjs.size) - 115;
        }
    }
    //sets a y val based on the piece type, to help center in the middle of the preview
    function drawYFunc(){
        if(p.type == "Line"){
            return (drawY * tetrjs.size) + -3;
        } else  if(p.type == "L" ){
            return (drawY * tetrjs.size) + 10;
        } else{
            return (drawY * tetrjs.size) + 25;
        }
    }



    this.previewCanvasCtx.clearRect(0,0,125,150);

    for(var r = 0, len = p.states[state].length; r < len; r++){
        for(var c = 0, len2 = p.states[state][r].length; c < len2; c++){

            //if part in array piece has a 1 and  gridy is > 0
            if(p.states[state][r][c] == 1 && drawY >= 0){
                this.previewCanvasCtx.drawImage(
                    this.blockImg,
                    p.color * this.size,
                    0,
                    this.size,
                    this.size,
                    drawXFunc(),
                    drawYFunc(),
                    this.size,
                    this.size
                );
            }
            drawX += 1;

        }
        drawX = p.gridx;
        drawY += 1;

    }

};


//Pauses or Plays the game by setting the pause bool;
NgTetrJS.prototype.pauseGame = function(){

    this.pause = true;
    this.updatePlayerStats();
    this.displayStateEl("pauseState");

};

//Plays Game
NgTetrJS.prototype.playGame = function(){

    document.getElementById("pauseState").style.display = "none";
    this.pause = false;

};

//resets everything
NgTetrJS.prototype.quitGame = function(){
    this.states().end();
};